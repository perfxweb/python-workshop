# Python Workshop

CEMS Club Vienna - From absolute 0 to Python hero

Workshop by Matthias Hausberger

# Content of this Gitlab repository
## sample_code
Just some sample code that we discussed in class. If you want me to add some more here or if you have code that you want to share with your colleagues, let me know via matthias.hausberger@s.wu.ac.at, I will upload it here.

## images folder
Just all memes and images used in the Colab Notebook.
